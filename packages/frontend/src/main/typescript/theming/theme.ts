import { Shadow } from "./properties/shadow"
import { ThemeStyles as ServiceListThemeStyles } from "../components/reusable/service_list/styles"
import { ThemeStyles as NavbarThemeStyles } from "../components/layout/navbar"
import { ThemeStyles as LayoutThemeStyles } from "../components/layout/layout"

export interface Theme {
	common: {
		shadows: {
			dark: {
				normal: Shadow
				inset: Shadow
			}
			glow: {
				normal: Shadow
			}
		}
	}
	components: {
		layout: LayoutThemeStyles
		navbar: NavbarThemeStyles
		bigButton: {
			width: string
			border: {
				width: string
			}
			icon: {
				width: string
				height: string
				margin: {
					bottom: string
				}
			}
			hoverAnimation: {
				duration: string
				easingFunction: string
				border: {
					width: string
				}
			}
		}
		serviceList: ServiceListThemeStyles
	}
}
