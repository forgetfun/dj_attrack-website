import { Point } from "./point"
import { CssProperty } from "./css_property"

export class Shadow implements CssProperty {
	public readonly blur: string = "0"
	public readonly size: string = "0"
	public readonly color: string = "hsl(0, 0%, 0%)"
	public readonly offset: Point = { x: "0", y: "0" }
	public readonly isInset: boolean = false

	public toFelaStyle() {
		return {
			boxShadow: `${this.isInset ? "inset " : ""}${this.offset.x} ${this.offset.y} ${this.blur} ${this.size} ${this.color}`,
		}
	}
}
