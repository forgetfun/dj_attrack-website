import { Service } from "../../components/pages/service"
import { withRouter, Router } from "next/dist/client/router"
import React from "react"

export default withRouter(
	class ServiceRoute extends React.Component<{
		router: Router
	}> {
		public render() {
			return <Service serviceId={this.props.router.query["id"] as string} />
		}
	},
)
