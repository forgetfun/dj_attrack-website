import React from "react"
import { Theme } from "../../theming/theme"
import { connect, Rules, FelaWithStylesProps } from "react-fela"
import { def } from "../../utils/default"
import { Person } from "../../models/contacts/person"

interface ContactListProps {
	people: Person[]
}

interface Styles {
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	personList: any
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	personName: any
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	contactMeanList: any
}

export const ContactList = connect((() => ({
	personList: {
		"& > :not(:first-child)": {
			marginTop: "1em",
		},
	},
	personName: {
		fontWeight: "bold",
	},
	contactMeanList: {
		paddingTop: "0.25em",
		paddingBottom: "0.25em",
		paddingLeft: "1em",
	},
})) as Rules<ContactListProps, Styles, Theme>)(
	class ContactList extends React.Component<ContactListProps & FelaWithStylesProps<ContactListProps, Styles, Theme>> {
		public render() {
			return (
				<div className={this.props.styles.personList}>
					{this.props.people.map(person => (
						<div key={person.key}>
							<div className={this.props.styles.personName}>{person.name}</div>
							<div className={this.props.styles.contactMeanList}>
								{person.contactMeans.map(contactMean => (
									<div key={contactMean.key}>
										{contactMean.type.name}: <a href={contactMean.url}>{def(contactMean.linkText, contactMean.url)}</a>
									</div>
								))}
							</div>
						</div>
					))}
				</div>
			)
		}
	},
)
