import React from "react"
import Link from "next/link"
import { connect, Rules, FelaWithStylesProps } from "react-fela"
import { Theme } from "../../../theming/theme"

export interface BigButtonProps {
	targetUrl: string
	iconUrl: string
	children: React.ReactNode
}

interface Styles {
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	link: any
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	icon: any
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	label: any
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	iconWrapper: any
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	iconBorderWrapper: any
}

export const BigButton = connect((props => ({
	link: {
		display: "block",
		width: props.theme.components.bigButton.width,
	},
	icon: {
		position: "absolute",
		width: props.theme.components.bigButton.icon.width,
		height: props.theme.components.bigButton.icon.height,
		objectFit: "contain",
	},
	label: {
		textAlign: "center",
		color: "white",
		fontSize: "1.2em",
		fontWeight: "bold",
	},
	iconWrapper: {
		position: "relative",
		display: "flex",
		justifyContent: "center",
		alignItems: "center",
		width: "100%",
		height: props.theme.components.bigButton.width,
		backgroundColor: "white",
		...props.theme.common.shadows.dark.normal.toFelaStyle(),
		borderRadius: "50%",
		"& .bigButton-iconBorderWrapper": {
			transition: `border-width ${props.theme.components.bigButton.hoverAnimation.duration} ${props.theme.components.bigButton.hoverAnimation.easingFunction}, transform ${props.theme.components.bigButton.hoverAnimation.duration} ${props.theme.components.bigButton.hoverAnimation.easingFunction}`,
		},
		":hover": {
			"& .bigButton-iconBorderWrapper": {
				borderWidth: props.theme.components.bigButton.hoverAnimation.border.width,
				transform: "rotate(1turn)",
			},
		},
	},
	iconBorderWrapper: {
		position: "absolute",
		width: "calc(100% + 2px)", // Add 2 pixels (one for left, one for right) to eliminate white gapsbetween border and background on the outer side
		height: "calc(100% + 2px)", // Add 2 pixels (one for top, one for bottom) to eliminate white gapsbetween border and background on the outer side
		borderTop: `${props.theme.components.bigButton.border.width} double black`,
		borderBottom: `${props.theme.components.bigButton.border.width} double black`,
		borderLeft: `${props.theme.components.bigButton.border.width} double black`,
		borderRight: `${props.theme.components.bigButton.border.width} solid black`,
		borderRadius: "50%",
	},
})) as Rules<BigButtonProps, Styles, Theme>)(
	class BigButton extends React.Component<BigButtonProps & FelaWithStylesProps<BigButtonProps, Styles, Theme>> {
		public render() {
			return (
				<Link href={this.props.targetUrl}>
					<a className={this.props.styles.link}>
						<div className={this.props.styles.iconWrapper}>
							<div className={`${this.props.styles.iconBorderWrapper} bigButton-iconBorderWrapper`} />
							<img src={this.props.iconUrl} className={this.props.styles.icon} />
						</div>
						<div className={this.props.styles.label}>{this.props.children}</div>
					</a>
				</Link>
			)
		}
	},
)
