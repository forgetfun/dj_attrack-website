import React from "react"
import { def } from "../../utils/default"
import { FelaComponent } from "react-fela"
import { letS } from "../../utils/scope_functions"

export enum Orientation {
	HORIZONTAL,
	VERTICAL,
}

interface SpacerProps {
	width: string
	orientation?: Orientation
}

export class Spacer extends React.Component<SpacerProps> {
	public render() {
		return (
			<FelaComponent
				style={{
					display: "inline-block",
					...letS(def(this.props.orientation, Orientation.VERTICAL), orientation => {
						switch (orientation) {
							case Orientation.HORIZONTAL:
								return {
									width: this.props.width,
									height: "100%",
								}
							case Orientation.VERTICAL:
								return {
									width: "100%",
									height: this.props.width,
								}
						}
					}),
				}}
			/>
		)
	}
}
