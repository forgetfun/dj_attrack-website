import { ImageModel } from "../../models/image"
import React from "react"

interface ImageProps {
	className?: string
	image: ImageModel
}

export class Image extends React.Component<ImageProps> {
	public render() {
		return <img className={this.props.className} src={this.props.image.url} alt={this.props.image.altText} />
	}
}
