import React from "react"
import App, { AppContext } from "next/app"
import Head from "next/head"
import { FelaProvider } from "./fela_provider"
import { IRenderer } from "fela"
import { ThemeProvider } from "react-fela"
import { baseTheme } from "../../theming/themes/base"
import { DataProviderContext } from "../../react_contexts/data_provider"
import { ServiceModel } from "../../models/service"
import { letS } from "../../utils/scope_functions"
import { ValueNotFoundError } from "../../errors/generic"
import { Person } from "../../models/contacts/person"
import { ContactMeanType } from "../../models/contacts/contact_mean"
import { ImageModel } from "../../models/image"
import { NumberRange } from "../../utils/range"

interface PageBaseProps {
	felaRenderer?: IRenderer
}

const services: ServiceModel[] = [
	{
		id: "party",
		name: "Párty",
		description:
			"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eleifend ipsum vel purus vulputate, a ullamcorper felis gravida. Aenean mauris ante, lacinia in ligula at, malesuada convallis ante. Nunc imperdiet in turpis quis vestibulum. Nullam malesuada egestas molestie. Cras leo ante, lobortis eu odio eget, rutrum rhoncus nisi. Duis auctor felis quis tempus semper. Sed interdum ullamcorper neque eu pretium. Fusce congue tellus et sem faucibus, sed tincidunt risus efficitur. Suspendisse id ligula maximus, dapibus enim volutpat, cursus est. Maecenas mauris eros, pharetra et accumsan non, congue ut odio. Praesent molestie nisi neque. Nam et ex vitae lectus mollis lobortis condimentum eget lacus. Aliquam sollicitudin sollicitudin massa nec mollis.",
		image: {
			url: "/static/images/service/party.jpg",
			altText: "Partys action",
		},
	},
	{
		id: "private_action",
		name: "Soukromé akce",
		description:
			"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam luctus pharetra libero sit amet pulvinar. Nunc feugiat eu ante eget sodales. Sed at est ut risus rutrum varius sit amet non ante. Donec nec massa vulputate nulla sagittis semper. Curabitur convallis vehicula justo ac posuere. Donec erat nisi, volutpat vel felis nec, suscipit rhoncus augue. Duis semper justo at consectetur sagittis. Sed vel rhoncus libero, id facilisis lacus. Vestibulum accumsan metus ac ligula tempus consectetur. Ut posuere rutrum odio, ut vestibulum dui scelerisque id. Fusce egestas justo in aliquet dapibus. Phasellus rutrum posuere tortor id venenatis. Ut faucibus vitae tellus sit amet varius. Suspendisse nec magna faucibus, maximus purus non, ullamcorper enim.",
		image: {
			url: "/static/images/service/private_action.jpg",
			altText: "Private actions icon",
		},
	},
	{
		id: "birthday",
		name: "Narozeninové párty",
		description:
			"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ac dapibus quam, nec mattis nibh. Aliquam laoreet dolor at consequat hendrerit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec eu elit ac ex dapibus faucibus quis eget est. Donec consectetur tellus quam, quis gravida sem bibendum auctor. Duis sit amet molestie arcu, vel rhoncus eros. Donec iaculis massa nisi, nec venenatis odio rutrum eget. Duis et ligula ac tortor vestibulum fermentum et eget purus. Duis id sem vulputate, ultricies lacus ac, posuere urna. Suspendisse potenti. Phasellus mollis consectetur diam eu sodales. Cras fringilla dolor ut faucibus condimentum. Fusce pellentesque ullamcorper risus, vitae sodales ex facilisis id. Curabitur rhoncus arcu in dapibus auctor. Nunc in imperdiet ligula, quis facilisis lorem. Sed nisl mauris, rutrum tincidunt eros quis, porttitor vestibulum felis.",
		image: {
			url: "/static/images/service/birthday.jpg",
			altText: "Birthday icon",
		},
	},
]

const contactMeanTypes: {
	[key: string]: ContactMeanType
} = {
	facebook: {
		name: "Facebook",
		key: "facebook",
	},
	instagram: {
		name: "Instagram",
		key: "instagram",
	},
}

const contacts: Person[] = [
	{
		name: "Adam Brožka",
		contactMeans: [
			{
				type: contactMeanTypes.facebook,
				url: "#adam_brozka-facebook",
				linkText: "Adam Brožka",
				key: "adam_brozka-facebook",
			},
			{
				type: contactMeanTypes.instagram,
				url: "#adam_brozka-instagram",
				linkText: "Adam Brožka",
				key: "adam_brozka-instagram",
			},
		],
		key: "adam_brozka",
	},
	{
		name: "Daniel Holý",
		contactMeans: [
			{
				type: contactMeanTypes.facebook,
				url: "#daniel_holy-facebook",
				linkText: "Daniel Holý",
				key: "daniel_holy-facebook",
			},
			{
				type: contactMeanTypes.instagram,
				url: "#daniel_holy-instagram",
				linkText: "Daniel Holý",
				key: "daniel_holy-instagram",
			},
		],
		key: "daniel_holy",
	},
]

const galleryImages: ImageModel[] = new NumberRange(1, 5).map(index => ({
	url: `/static/images/slider/slides/0${index}.jpeg`,
	altText: `Gallery image #${index}`,
}))

export class PageBase extends App<PageBaseProps> {
	public static async getInitialProps({ Component, ctx }: AppContext) {
		return {
			pageProps: Component.getInitialProps ? await Component.getInitialProps(ctx) : {},
		}
	}
	public render() {
		const { Component, pageProps } = this.props
		return (
			<>
				<Head>
					{/* <link
						rel="stylesheet"
						href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
						integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
						crossOrigin="anonymous"
					/> */}
					{/* Fonts */}
					<link href="https://fonts.googleapis.com/css?family=Montserrat|Roboto+Slab|Staatliches&display=swap" rel="stylesheet" />
				</Head>
				<FelaProvider renderer={this.props.felaRenderer}>
					<ThemeProvider theme={baseTheme}>
						<DataProviderContext.Provider
							value={{
								getServices: () => services,
								getServiceById: id =>
									letS(
										services.find(service => service.id == id),
										service => {
											if (service == undefined) throw new ValueNotFoundError()
											return service
										},
									),
								getContacts: () => contacts,
								getGalleryImages: () => galleryImages,
							}}
						>
							<Component {...pageProps} />
						</DataProviderContext.Provider>
					</ThemeProvider>
				</FelaProvider>
			</>
		)
	}
}
