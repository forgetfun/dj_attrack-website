import React from "react"
import { Navbar } from "./navbar"
import { FelaComponent, StyleProps } from "react-fela"
import { Theme } from "../../theming/theme"
import { Shadow } from "../../theming/properties/shadow"
import { BoundingBox } from "../../theming/properties/bounding_box"
// import { flatMapObjectArray } from "../../utils/array"

// styles {

import "../../../stylus/components/layout.styl"
import Head from "next/head"

// }

export interface ThemeStyles {
	root: {
		background: {
			color: string
			image: string
		}
		text: {
			color: string
		}
		content: {
			width: string
			bounds: BoundingBox[]
			shadow: Shadow
		}
	}
	composition: {
		navbar: {
			height: string
		}
	}
}

interface LayoutProps {
	children: React.ReactNode
	pageTitle?: string
}

export class Layout extends React.Component<LayoutProps> {
	public render() {
		return (
			<>
				<Head>
					<title>{this.props.pageTitle === undefined ? "DJ Attract" : `${this.props.pageTitle} | DJ Attract`}</title>
				</Head>
				<FelaComponent
					style={(props: StyleProps<Theme>) => ({
						backgroundColor: props.theme.components.layout.root.background.color,
						backgroundImage: props.theme.components.layout.root.background.image,
						color: props.theme.components.layout.root.text.color,
						width: "100%",
						height: "100%",
						maxWidth: "100vw",
						minHeight: "100vh",
						display: "flex",
						flexDirection: "column",
						alignItems: "center",
						// backgroundColor: "hsla(0, 0%, 15%, 0.5)",
					})}
				>
					<FelaComponent
						style={(props: StyleProps<Theme>) => ({
							width: "100%",
							height: props.theme.components.layout.composition.navbar.height,
							position: "sticky",
							top: "0",
							zIndex: "5",
						})}
					>
						{({ className }) => (
							<header className={className}>
								<Navbar
									sections={[
										{
											id: "home",
											name: "Domů",
											url: "/",
											icon: {
												url: "/static/images/buttons/icons/light/home.svg",
												altText: "home icon",
											},
										},
										{
											id: "calendar",
											name: "Kalendář",
											url: "/calendar",
											icon: {
												url: "/static/images/buttons/icons/light/calendar.svg",
												altText: "calendar icon",
											},
										},
										{
											id: "gallery",
											name: "Galerie",
											url: "/gallery",
											icon: {
												url: "/static/images/buttons/icons/light/gallery.svg",
												altText: "gallery icon",
											},
										},
									]}
								/>
							</header>
						)}
					</FelaComponent>
					{this.props.children}
				</FelaComponent>
			</>
		)
	}
}
