import React from "react"
import { Layout } from "../layout/layout"
import { FelaComponent } from "react-fela"
import { DataProviderContext } from "../../react_contexts/data_provider"
import { fonts } from "../../styles/fonts"
import { Spacer } from "../reusable/spacer"
import { ContactList } from "../reusable/contact_list"
import { container } from "../../styles/layout"

interface ServiceProps {
	serviceId: string
}

export class Service extends React.Component<ServiceProps> {
	public render() {
		return (
			<DataProviderContext.Consumer>
				{dataProvider => {
					const service = dataProvider.getServiceById(this.props.serviceId)
					return (
						<Layout>
							<FelaComponent style={container}>
								<Spacer width="4em" />
								<FelaComponent
									style={{
										width: "100%",
										textAlign: "center",
										fontSize: "3em",
										...fonts.robotoSlab,
									}}
								>
									{service.name}
								</FelaComponent>
								<Spacer width="1em" />
								{service.description}
								<Spacer width="4em" />
								<ContactList people={dataProvider.getContacts()} />
							</FelaComponent>
						</Layout>
					)
				}}
			</DataProviderContext.Consumer>
		)
	}
}
