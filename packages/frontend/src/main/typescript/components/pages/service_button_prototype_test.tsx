import React from "react"
import { Layout } from "../layout/layout"
import { FelaComponent } from "react-fela"
import { ServiceButton } from "../reusable/service_list/components/service_button_prototype"

export class ServiceButtonPrototypeTest extends React.Component {
	public render() {
		return (
			<Layout>
				<FelaComponent
					style={{
						width: "100%",
						display: "flex",
						justifyContent: "space-evenly",
					}}
				>
					<ServiceButton
						service={{
							id: "party",
							name: "Párty",
							description: "Párty popisek",
							image: {
								// url: "https://875142e8f3271689e62a-a139377f76f0cc62566552bd9a03d393.ssl.cf3.rackcdn.com/image/content/1280x1024/e43cae2a916da5d3d0e513e2141a6fcf.jpg",
								url: "/static/images/buttons/icons/gallery.svg",
								altText: "",
							},
						}}
						targetUrl="/service/party"
					/>
					<ServiceButton
						service={{
							id: "birthday",
							name: "Narozeniny",
							description: "Narozeniny popisek",
							image: {
								// url: "https://875142e8f3271689e62a-a139377f76f0cc62566552bd9a03d393.ssl.cf3.rackcdn.com/image/content/1280x1024/75eab7a5c080d31aecb9e1a832997de2.jpg",
								url: "/static/images/buttons/icons/gallery.svg",
								altText: "",
							},
						}}
						targetUrl="/service/birthday"
					/>
				</FelaComponent>
			</Layout>
		)
	}
}
