import { FelaStyleObject } from "../utils/fela/types"

export const effects: Record<"shine", FelaStyleObject> = {
	shine: {
		position: "relative",
		overflow: "hidden",
		"::after": {
			content: '" "',
			display: "block",
			position: "absolute",
			width: "150%",
			height: "150%",
			top: "-75%",
			left: "-75%",
			opacity: 0,
			background: "linear-gradient(135deg, hsla(0,0%,0%,0) 0%, hsla(0,0%,0%,0) 45%, hsla(0,0%,100%,0.2) 47.5%, hsla(0,0%,0%,0) 55%, rgba(0,0,0,0) 0%)",
		},
		":hover": {
			"::after": {
				opacity: 1,
				top: "75%",
				left: "75%",
				transition: "left 1s ease, top 1s ease, opacity 0.15s ease",
			},
		},
	},
}
