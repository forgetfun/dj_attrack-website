import { FelaStyleObject } from "../utils/fela/types"

export const normalization: Record<"button" | "link", FelaStyleObject> = {
	button: {
		display: "inline-block",
		border: "none",
		padding: "0",
		margin: "0",
		textDecoration: "none",
		background: "transparent",
		color: "inherit",
		fontFamily: "inherit",
		fontSize: "inherit",
		cursor: "pointer",
		textAlign: "center",
		appearance: "none",
		":hover": {
			background: "transparent",
		},
	},
	link: {
		color: "inherit",
		textDecoration: "none",
		":hover": {
			color: "inherit",
			textDecoration: "none",
		},
	},
}
