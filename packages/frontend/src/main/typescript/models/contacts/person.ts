import { ContactMean } from "./contact_mean"

export interface Person {
	name: string
	contactMeans: ContactMean[]
	key: string
}
