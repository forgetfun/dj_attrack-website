export const flatMapObjectArray = <ItemType, ResultType>(array: ItemType[], predicate: (value: ItemType, index: number, array: ItemType[]) => ResultType) =>
	Object.assign({}, ...array.map(predicate)) as ResultType
