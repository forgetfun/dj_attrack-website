declare global {
	// eslint-disable-next-line @typescript-eslint/no-namespace
	namespace NodeJS {
		interface Process {
			browser?: boolean
		}
	}
}

export const isClient = process.browser !== undefined && process.browser
