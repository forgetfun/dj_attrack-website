export const letS = <ValueType, ReturnType>(value: ValueType, predicate: (value: ValueType) => ReturnType) => predicate(value)

export const runS = <ValueType, ReturnType>(value: ValueType, predicate: (this: ValueType) => ReturnType) => predicate.apply(value)

export const applyS = <ValueType>(value: ValueType, predicate: (value: ValueType) => void) => (predicate(value), value)

export const alsoS = <ValueType>(value: ValueType, predicate: (this: ValueType) => void) => (predicate.apply(value), value)
