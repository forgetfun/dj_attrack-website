import * as Fela from "fela"
import felaPresetWeb from "fela-preset-web"

export class FelaRendererFactory {
	public static getNewFelaRenderer() {
		return Fela.createRenderer({
			plugins: [...felaPresetWeb],
		})
	}
}
