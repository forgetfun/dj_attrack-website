import { IStyle } from "fela"
import { allowedKeyframeKeys } from "./generated/allowed_keyframe_keys"
import { StyleProps } from "react-fela"

type pseudoElement = "::before" | "::after"
type pseudoClass = ":hover"

export type FelaStyleObject<ResultType = {}> = IStyle &
	Partial<
		{
			[key in pseudoElement | pseudoClass]: FelaStyleObject
		}
	> &
	ResultType

type FelaStyleFunction<ThemeType = {}, PropsType = {}, ResultType = {}> = (styleProps: StyleProps<ThemeType, PropsType>) => FelaStyleObject<ResultType>

export type FelaStyle<ThemeType = {}, PropsType = {}, ResultType = {}> = FelaStyleObject<ResultType> | FelaStyleFunction<ThemeType, PropsType, ResultType>

export type KeyFrames<Props = {}> = (props: Props) => Partial<Record<allowedKeyframeKeys, FelaStyleObject>>

export const allowUnsafeFelaStyleKeys = <P>(initializer: FelaStyle & P) => initializer

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const allowUnsafeFelaStyleKeysOnRecord = <KeyType extends keyof any, P>(initializer: Record<KeyType, FelaStyle & P>) => initializer
