import { def } from "./default"

interface Range<PointType> {
	readonly from: PointType
	readonly to: PointType
	forEach(
		callbackfn: (value: PointType, range: Range<PointType>) => void,
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		thisArg?: any,
	): void
	map<ResultType>(
		callbackfn: (value: PointType, range: Range<PointType>) => ResultType,
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		thisArg?: any,
	): ResultType[]
}

export class NumberRange implements Range<number> {
	public readonly from: number
	public readonly to: number
	public readonly step: number
	public constructor(from: number, to: number, step?: number) {
		this.from = from
		this.to = to
		this.step = def(step, 1)
	}
	public forEach(
		callbackfn: (value: number, range: Range<number>) => void,
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		thisArg?: any,
	) {
		const progressFunction: { (i: number): number } = this.from < this.to ? i => i + this.step : i => i - this.step
		for (let i = this.from; i <= this.to; i = progressFunction(i)) callbackfn.apply(thisArg, [i, this])
	}
	public map<ResultType>(
		callbackfn: (value: number, range: Range<number>) => ResultType,
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		thisArg?: any,
	) {
		const accumulated: ResultType[] = []
		this.forEach(i => {
			accumulated.push(callbackfn.apply(thisArg, [i, this]))
		})
		return accumulated
	}
}
;[].map
